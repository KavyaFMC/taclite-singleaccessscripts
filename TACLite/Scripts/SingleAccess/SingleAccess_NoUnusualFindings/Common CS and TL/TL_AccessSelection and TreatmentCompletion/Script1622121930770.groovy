import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import utilityKeywords.helperUtilities as helperUtilities
import com.kms.katalon.core.testdata.reader.ExcelFactory as ExcelFactory
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.testobject.ConditionType as ConditionType

Properties prop = helperUtilities.getTheObject('C:\\katalon\\TACLite\\Data Resources\\TacLite_Properties\\AddAccess.properties')

Properties prop1 = helperUtilities.getTheObj('C:\\katalon\\TACLite\\Data Resources\\ChairSide_Properties\\EnvironmentalVariables.properties')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Access_Eval'), '', 'click')

WebUI.waitForPageLoad(5)

WebUI.takeScreenshot()

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Frame_PreDialysis'), '', 'switchframe')

CustomKeywords.'pageLocators.pageOperation.pageAction'('', '', 'defaultcontent')

WebUI.delay(2)

CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('TAC_Frame'), '', 'switchframe')

Object excelData = ExcelFactory.getExcelDataWithDefaultSheet('C://katalon//TACLite//InputData//Test.xlsx', 'BothNeedle_NoFindings', 
    true)

def AccessStatus1 = excelData.getValue('AccessType1', 1)

Access = AccessStatus1.substring(0, 3)

println(Access)

TestObject testObj = new TestObject()

AVGAccess = 'AVG'
AVFAccess = 'AVF'
CVCAccess = 'CVC'

if (WebUI.verifyEqual(Access, AVGAccess, FailureHandling.OPTIONAL)) {
    println('This is AVG access')

    CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select1'), '', 'click')


	def LineSource1=excelData.getValue('LineSource1', 1)
	println(LineSource1)
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource1+prop.getProperty('Add'), '', 'click')
	
    CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')

    CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

    WebUI.delay(2)

    WebUI.takeScreenshot()

    WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-PreTx'), [:], FailureHandling.STOP_ON_FAILURE)

	WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/Common CS and TL/CS_StartTx'), [:], FailureHandling.STOP_ON_FAILURE)
	
    WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-CannDoc'), [:], FailureHandling.STOP_ON_FAILURE)

	WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-AVG MattCann'), [:],
		FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-AFT'), [:], FailureHandling.STOP_ON_FAILURE)
	
    WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-DuringPost'), [:], 
        FailureHandling.STOP_ON_FAILURE)

   
	WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/Common CS and TL/CS_PostDialysis'), [:],
		FailureHandling.STOP_ON_FAILURE)
	
	} 
	
	else if (WebUI.verifyEqual(Access, AVFAccess, FailureHandling.OPTIONAL)) {
    println('This is AVF access')
	
    CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select1'), '', 'click')


	def LineSource1=excelData.getValue('LineSource1', 1)
	println(LineSource1)
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource1+prop.getProperty('Add'), '', 'click')
		
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)

WebUI.takeScreenshot()

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-PreTx'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/Common CS and TL/CS_StartTx'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-CannDoc'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-AVF MattCann'), [:],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-AFT'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/AVG and AVF access/TL-DuringPost'), [:],
	FailureHandling.STOP_ON_FAILURE)


WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/Common CS and TL/CS_PostDialysis'), [:],
	FailureHandling.STOP_ON_FAILURE)
		
} else if (WebUI.verifyEqual(Access, CVCAccess, FailureHandling.OPTIONAL)) {
    println('This is CVC access')
	
    CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Select1'), '', 'click')
	

	def LineSource1=excelData.getValue('LineSource1', 1)
	println(LineSource1)
	
	CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('Arterial')+LineSource1+prop.getProperty('Add'), '', 'click')
	
CustomKeywords.'pageLocators.pageOperation.pageAction'(prop.getProperty('OKbutton'), '', 'click')

CustomKeywords.'reusablekeyword.Reusable.TACSignature'()

WebUI.delay(2)

WebUI.takeScreenshot()
	
WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/CVC access/TL-PreTx'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/Common CS and TL/CS_StartTx'), [:], FailureHandling.STOP_ON_FAILURE)


WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/CVC access/TL-DuringPost'), [:],
	FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('SingleAccess/SingleAccess_NoUnusualFindings/Common CS and TL/CS_PostDialysis'), [:],
	FailureHandling.STOP_ON_FAILURE)
} 

else {
    println('This access doesnot fall into options that can be used for treatment')
	
  }

